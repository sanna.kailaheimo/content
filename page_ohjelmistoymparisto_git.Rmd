---
title: "Git-ohjeita"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: yes
    code_folding: show
---


![Hahmotelma siitä, mikä on upstream (utur2016/content), mikä origin (sinun_utu_tunnus/content) ja mikä paikallinen kone (omakone tai csc:n poutapilvi)](http://i.stack.imgur.com/LtFGa.png)

